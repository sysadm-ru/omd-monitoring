#!/bin/bash

# Update hosts file
echo "[TASK 1] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
192.168.0.11 ubuntuvm01.example.com ubuntuvm01
192.168.0.12 ubuntuvm02.example.com ubuntuvm02
192.168.0.21 centosvm01.example.com centosvm01
192.168.0.21 centosvm02.example.com centosvm02
EOF
